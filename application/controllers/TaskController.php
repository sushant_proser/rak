<?php
class TaskController extends CI_Controller {



  public function index()
  {
    $this->load->view('TaskView');
  }



// Insert data function

 public function InsertData(){

   //Setting validation rules
   $this->form_validation->set_rules('name','name ','required');
   $this->form_validation->set_rules('run','name ','required');
   $this->form_validation->set_rules('team','name ','required');

   if($this->form_validation->run())
   {
       $name=$this->input->post('name');
       $run=$this->input->post('run');
       $team=$this->input->post('team');

       //loading model
       $this->load->model('Task_Model');
       $this->Task_Model->insertdata($name,$run,$team);
       $this->load->view('TaskView',$data);
   } else {
         // for error chechking in input
      // $this->session->set_flashdata('error', 'Please check your Input value!!');
       $this->load->view('TaskView');
   }
 }


// Update/Edit data function

public function EditData(){

  $id=$this->input->post('id');
  $name=$this->input->post('name');
  $run=$this->input->post('run');
  $team=$this->input->post('team');
  $this->load->model('Task_Model');
	$this->Task_Model->editdata($id,$name,$run,$team);
  $this->load->view('TaskView',$data);

}
// Delete Data Function

public function DeleteData(){

  $id=$this->uri->segment(2);
  // we can also take id from form data using hidden field
	//  $id=$this->input->post('id');

	$this->load->model('Task_Model');
	$this->Task_Model->deletedata($id);
  $this->load->view('TaskView',$data);

}
// Retrieve data function
public function FetchData(){

    $this->load->model('Task_Model');
  	$data['taskdata']=$this->Task_Model->fetchdata();
  	//$this->load->view('TaskView',$data);
    $this->load->view('welcome_message',$data);


}

// Reamaining any function as per task
public function CommonFunction(){


}




}
?>
